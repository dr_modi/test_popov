Rails.application.routes.draw do
  #get 'project/index'
  get "/projects", to: "project#index"
  post "/todos", to: "project#creat"
  patch "/projects/:id/todo/:id", to: "project#update"

  resources :categories do
    resources :tasks
  end
end
