# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Home = Category.create({id:1, title: "Дом"})
Work = Category.create({id:2, title: "Работа"})
Other = Category.create({id:3, title: "Прочее"})

Home.tasks.create({text: "Купить молоко", isCompleted: false})
Home.tasks.create({text: "Заплатить за квартиру", isCompleted: false})
Work.tasks.create({text: "Позвонить клиенту", isCompleted: false})
Work.tasks.create({text: "Подготовить отчет", isCompleted: false})
Other.tasks.create({text: "Позвонить другу", isCompleted: false})
Other.tasks.create({text: "Постирать постельное", isCompleted: false})