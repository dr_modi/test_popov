class Task < ApplicationRecord
  belongs_to :category
  validates :text, presence: true, uniqueness: true
end
