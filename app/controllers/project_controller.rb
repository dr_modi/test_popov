class ProjectController < ApplicationController
  def index
    @categories=Category.all.select([:id, :title])
    @tasks=[]
    @categories.each do |task|
      @tasks << task.tasks.select([:id, :text, :isCompleted])
    end
  end

  def creat
    category = Category.find(params[:category_id])
    if category
      task = Category.find(params[:category_id]).tasks.new([:text, :isCompleted])
      if task.save
        redirect_to index
      else
        render json: {errors: task.errors}, status: :unprocessable_entity
      end
    else
      cat_tsk = Category.new([:title]).tasks.new([:text, :isCompleted])
      if cat_tsk.save
        redirect_to index
      else
        render json: {errors: cat_tsk.errors}, status: :unprocessable_entity
      end
    end
  end

  def update
    task = Task.find(params[:id])
    if task.update(params[:isCompleted])
      redirect_to index
    else
      render json: {errors: task.errors}, status: :unprocessable_entity
    end
  end

end
